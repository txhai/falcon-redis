FROM redis:5.0-alpine
RUN apk add --no-cache bash
COPY docker-healthcheck /usr/local/bin/
RUN chmod a+x /usr/local/bin/docker-healthcheck
HEALTHCHECK CMD ["docker-healthcheck"]
